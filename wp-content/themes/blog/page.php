<?php
/**
 * Created by PhpStorm.
 * User: Henry Jr
 * Date: 21/03/2016
 * Time: 16:46
 */
?>

<?php get_header(); ?>

<section id="post" class="row">
    <div class="col-md-8">
        <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
            <?php get_template_part('content', 'page'); ?>
            <?php //comments_template('', true); ?>
        <?php endwhile; else: ?>
            <article class="not-found">
                <p><?php _e('Desculpe, nenhum post corresponde aos seus crit&eacute;rios.', ''); ?></p>
            </article>
        <?php endif; ?>
    </div>
    <div class="col-md-4">
        <?php if (function_exists('pf_show_link')) {
            echo pf_show_link();
        } ?>
        <?php get_sidebar(); ?>
    </div>
    <br style="clear:both">
</section>
<span class="space-150"></span>
</div>

<?php get_footer(); ?>
