<?php
/**
 * The template for displaying single content. Used for single.
 *
 * @package WordPress
 * @subpackage wbruno
 * @since wbruno 0.0.1
 */
?>

<article <?php post_class() ?> id="post-<?php the_ID(); ?>" itemscope itemtype="http://schema.org/Article" role="article">
    <header role="banner" class="posthome">
        <?php echo lw_date(); ?>
        <h1 class="entry-title" role="heading" aria-level="2" itemprop="name">
            <a href="<?php the_permalink(); ?>" alt="<?php the_title(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a>
        </h1>
        <small class="fleft">
            <span class="label label-danger">
                <?php edit_post_link(__('<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Editar', '')); ?></span>
            <span class="vcard author" itemprop="author" itemscope itemtype="http://schema.org/Person">
                By <span class="fn" itemprop="name"><?php the_author_link(); ?></span>
            </span> | Categoria: <span itemprop="articleSection"><?php the_category(', ') ?></span> |
            <span class="label label-default" type="button">
                Comentários <span class="badge"><?php comments_popup_link('0', '1', '%', 'comments'); ?></span>
            </span>
        </small>
    </header>
    <div id="post" class="post-content entry-content posthome">
        <span class="thumbnails"><?php the_post_thumbnail('news'); ?></span>
        <span class="space-25"></span>
        <section class="post-container">
            <?php the_content('Leia &raquo; ' . get_the_title()); ?>
        </section>
    </div>
    <!-- .post-content -->
    <footer role="contentinfo">
        <p>
            <?php the_tags(); ?>
        </p>
        <?php wp_link_pages(); ?>
    </footer>
</article>
<!-- .post --> 
