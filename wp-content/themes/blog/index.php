<?php
/**
 * Created by PhpStorm.
 * User: Henry Jr
 * Date: 21/03/2016
 * Time: 16:46
 */
?>

<?php get_header(); ?>

<!-- Carousel
================================================== -->
<?php include('inc/carousel.php'); ?>

<!-- Destqaques
================================================== -->
<?php include('inc/destaques.php'); ?>

<!-- blog e sidebar
================================================== -->
<?php include('inc/blog.php'); ?>

<?php get_footer(); ?>
