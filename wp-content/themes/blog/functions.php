<?php
/**
 * Created by PhpStorm.
 * User: Henry Jr
 * Date: 21/03/2016
 * Time: 16:46
 */

// inclusão de css e js
function wp_style()
{
    wp_enqueue_style('style', get_template_directory_uri() . '/style.css', true, '1.0', 'all');
    wp_enqueue_style('lato', 'https://fonts.googleapis.com/css?family=Lato:400,400italic,700,700italic', true);
    wp_enqueue_style('dancing', 'https://fonts.googleapis.com/css?family=Dancing+Script:400,700', true);
    wp_enqueue_script('functions', get_template_directory_uri() . '/js/functions.js', array(), '1.0.0', true);
}

add_action('wp_enqueue_scripts', 'wp_style');


// exclusao de tags css do worpress
function remove_admin_login_header()
{
    remove_action('wp_head', '_admin_bar_bump_cb');
}

add_action('get_header', 'remove_admin_login_header');


// edição do menu nav do bootstrap para o wordpress
/* Theme setup */
add_action('after_setup_theme', 'wpt_setup');
if (!function_exists('wpt_setup')):
    function wpt_setup()
    {
        register_nav_menu('primary', __('Primary navigation', 'wptuts'));
        register_nav_menu('secundary', __('Secundary navigation', 'wptuts'));
        register_nav_menu('social', __('Social navigation', 'wptuts'));
    } endif;

add_action('widgets_init', 'widgets_init');
function widgets_init()
{
    register_sidebar(array(
        'id' => 'facebook',
        'name' => __('facebook', 'blog'),
        'description' => __('Sidebar Facebook.', 'blog'),
    ));
    register_sidebar(array(
        'id' => 'instagram',
        'name' => __('instagram', 'blog'),
        'description' => __('Sidebar Instagram.', 'blog'),
    ));
    register_sidebar(array(
        'id' => 'stats',
        'name' => __('stats', 'blog'),
        'description' => __('Sidebar stats.', 'blog'),
    ));
    register_sidebar(array(
        'id' => 'newsletter',
        'name' => __('newsletter', 'blog'),
        'description' => __('Sidebar stats.', 'blog'),
    ));
}

/**
 * lw_date
 */
function lw_date()
{
    global $post;
    $ts = strtotime($post->post_date);
    return '<div class="date"><span class="d">' . get_the_time('d') . '</span><span class="m">' . get_the_time('M') . '</span><span class="y">' . get_the_time('Y') . '</span></div>' . PHP_EOL;
    //return '<time datetime="' . get_the_time('Y-m-d') . '" class="updated">' . get_the_time('d') . ' <span>' . get_the_time('M') . ' ' . get_the_time('Y') . '</span></time>' . PHP_EOL;
}

/**
 * overwrite wp_list_categories
 */
add_filter('next_posts_link_attributes', 'get_next_posts_link_attributes');
add_filter('previous_posts_link_attributes', 'get_previous_posts_link_attributes');

if (!function_exists('get_next_posts_link_attributes')) {
    function get_next_posts_link_attributes($attr)
    {
        $attr = 'rel="next" title="Posts mais antigos"';
        return $attr;
    }
}
if (!function_exists('get_previous_posts_link_attributes')) {
    function get_previous_posts_link_attributes($attr)
    {
        $attr = 'rel="prev" title="Posts mais recentes"';
        return $attr;
    }
}

/**
 * Add a rel="nofollow" to the comment reply links
 */
function add_nofollow_to_reply_link($link)
{
    return str_replace('")\'>', '")\' rel=\'nofollow\'>', $link);
}

add_filter('comment_reply_link', 'add_nofollow_to_reply_link');

if (!function_exists('parent_comment')) :
    /**
     * Template for comments and pingbacks.
     *
     * To override this walker in a child theme without modifying the comments template
     * simply create your own twentyten_comment(), and that function will be used instead.
     *
     * Used as a callback by wp_list_comments() for displaying the comments.
     *
     * @since wbruno 0.0.1
     */
    function parent_comment($comment, $args, $depth)
    {
        $GLOBALS['comment'] = $comment;
        switch ($comment->comment_type) :
            case '' :
                ?>

                <li <?php comment_class(); ?>
                    id="comment-<?php comment_ID() ?>"> <?php echo get_avatar($comment, 32); ?> <cite>
                        <?php comment_text() ?>
                    </cite>
                    <?php _e('por', 'wbruno'); ?>
                    <?php comment_author_link() ?>
                    &#8212;
                    <time datetime="<?php echo $comment->comment_date; ?>">
                        <?php comment_date() ?>
                        @ <a href="#comment-<?php comment_ID() ?>" rel="nofollow">
                            <?php comment_time() ?>
                        </a></time>
                    <?php edit_comment_link(__('Editar', 'wbruno'), ' | '); ?>
                    <?php if ($comment->comment_approved == '0') : ?>
                        <em class="comment-awaiting-moderation">
                            <?php _e('Seu coment&aacute;rio est&aacute; aguardando revis&atilde;o', 'wbruno'); ?>
                        </em>
                    <?php endif; ?>
                    <?php comment_reply_link(array_merge($args, array('depth' => $depth, 'max_depth' => $args['max_depth']))); ?>
                </li>
                <?php
                break;
            case 'pingback'  :
            case 'trackback' :
                ?>
                <li class="post pingback">
                <p>
                    <?php _e('Pingback:', 'wbruno'); ?>
                    <?php comment_author_link(); ?>
                    <?php edit_comment_link(__('(Editar)', 'wbruno'), ' '); ?>
                </p>
                <?php
                break;
        endswitch;
    }
endif;


//Personal Edit
if (function_exists('add_theme_support'))
    add_theme_support('post-thumbnails');
if (function_exists('add_image_size')) {
    add_image_size('original', 9999, 9999, false);
    add_image_size('slider', 822, 330, true);
    add_image_size('thumb', 205, 150, true);
    add_image_size('news', 600, 260, true);
    add_image_size('bnr', 310, 100, true);
}
function excerpt($limit)
{
    $excerpt = explode(' ', get_the_excerpt(), $limit);
    if (count($excerpt) >= $limit) {
        array_pop($excerpt);
        $excerpt = implode(" ", $excerpt) . ' [+]';
    } else {
        $excerpt = implode(" ", $excerpt);
    }
    $excerpt = preg_replace('`\^\*\]`', '', $excerpt);
    return $excerpt;
}

function the_titlesmall($before = '', $after = '', $echo = true, $length = false)
{
    $title = get_the_title();
    if (strlen($title) < $length) {
        echo $title;
    } else {
        if ($length && is_numeric($length)) {
            $title = substr($title, 0, $length);
        }
        if (strlen($title) > 0) {
            $title = apply_filters('the_titlesmall', $before . $title . $after, $before, $after);
            if ($echo)
                echo $title;
            else
                return $title;
        }
    }
}


@ini_set('upload_max_size', '64M');
@ini_set('post_max_size', '64M');
@ini_set('max_execution_time', '300');

function wpbeginner_remove_version()
{
    return '';
}

add_filter('the_generator', 'wpbeginner_remove_version');
remove_action('wp_head', 'wp_generator');
add_filter('jetpack_enable_open_graph', '__return_false', 99);

// turn Photon off so we can get the correct image
$photon_removed = '';
if (class_exists('Jetpack') && Jetpack::is_module_active('photon')) { // check that we are, in fact, using Photon in the first place
    $photon_removed = remove_filter('image_downsize', array(Jetpack_Photon::instance(), 'filter_image_downsize'));
}
// do things with image functions
// turn Photon back on
if ($photon_removed) {
    add_filter('image_downsize', array(Jetpack_Photon::instance(), 'filter_image_downsize'), 10, 3);
}
function wpb_imagelink_setup()
{
    $image_set = get_option('image_default_link_type');

    if ($image_set !== 'none') {
        update_option('image_default_link_type', 'none');
    }
}

add_action('admin_init', 'wpb_imagelink_setup', 10);


// Custom WordPress Login Logo
function my_login_logo()
{ ?>
    <style>
        body.login {
            background: #ffffff url("/wp-content/themes/blog/images/bgHeader.jpg") top center;
        }

        body.login div#login h1 a {
            background: url(/wp-content/themes/blog/images/PatyPiih.png) no-repeat 50% 50%;
            width: 320px;
            height: 138px;
            background-size: 320px;
        }
    </style>

<?php }

add_action('login_enqueue_scripts', 'my_login_logo');

// Customizar o Footer do WordPress
function remove_footer_admin()
{
    echo '© Agência Pipa - 2016';
}

add_filter('admin_footer_text', 'remove_footer_admin');