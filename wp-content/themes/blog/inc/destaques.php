<div class="row">
    <div class="container text-center destaque">

        <!-- look do dia -->
        <?php include('look-do-dia.php'); ?>

        <!-- dicas do dia -->
        <?php include('dicas-de-moda.php'); ?>

        <!-- curiosidades -->
        <?php include('curiosidades.php'); ?>

    </div>
</div>

<!-- Espaçamento
================================================== -->
<div class="space-50"></div>