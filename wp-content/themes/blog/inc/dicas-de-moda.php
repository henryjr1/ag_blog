<?php query_posts('category_name=dicas&posts_per_page=1'); ?>
<?php if (have_posts()) : while (have_posts()) :
    the_post(); ?>
    <div class="col-sm-4">
        <a href="<?php the_permalink(); ?>" alt="<?php the_title(); ?>" title="<?php the_title(); ?>">
            <span></span>
            <?php the_post_thumbnail('thumb'); ?>
        </a>
        <h3><a href="<?php the_permalink(); ?>">Dicas de moda</a></h3>
        <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/lineFoto.png">
    </div>
<?php endwhile; ?>
<?php else: ?>
<?php endif; ?>