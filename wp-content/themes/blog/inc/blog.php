<div class="row">
    <div class="container">
        <!-- Coluna blog
        ================================================== -->
        <div class="col-sm-8">
            <?php query_posts('category_name=blog&posts_per_page=10'); ?>
            <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                <div class="posthome">
                    <!-- data -->
                    <?php echo lw_date(); ?>
                    <!-- title -->
                    <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                    <!-- img -->
                    <a href="<?php the_permalink(); ?>" class="thumbnails"><?php the_post_thumbnail('news'); ?></a>
                    <!-- social -->
                    <a rel="nofollow" data-shared="sharing-facebook" class="share-facebook sd-button social"
                       href="<?php the_permalink() ?>?share=facebook" target="_blank" title="Clique para compartilhar no Facebook"><img
                            src="<?php echo esc_url(get_template_directory_uri()); ?>/images/fb.jpg"></a></a>
                    <!-- content / limit 200 -->
                    <p class="chamada"><?php echo excerpt(200); ?></p>
                    <span class="space-50"></span>
                </div>
            <?php endwhile; ?>
            <?php else: ?>
            <?php endif; ?>
        </div>
        <!-- Coluna sidebar
        ================================================== -->
        <div class="col-sm-4">
            <?php get_sidebar(); ?>
        </div>
    </div>
</div>
<span class="space-150"></span>
<script type="text/javascript">
    var windowOpen;
    jQuery(document).on('ready post-load', function () {
        jQuery('a.share-facebook').on('click', function () {
            if ('undefined' !== typeof windowOpen) { // If there's another sharing window open, close it.
                windowOpen.close();
            }
            windowOpen = window.open(jQuery(this).attr('href'), 'wpcomfacebook', 'menubar=1,resizable=1,width=600,height=400');
            return false;
        });
    });
</script>