<div class="row">
    <div class="mascara">
        <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/mascaraSlider.png">
    </div>
    <div id="myCarousel" class="carousel slide carousel-fade" data-ride="carousel">
        <!-- Indicators -->
        <ol class="carousel-indicators">

            <?php query_posts('category_name=slider&posts_per_page=1'); ?>
            <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                <li data-target="#carousel-example-generic" data-slide-to="2"></li>
            <?php endwhile; ?>
            <?php else: ?>
            <?php endif; ?>

        </ol>

        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox">
            <?php query_posts('category_name=slider&posts_per_page=1'); ?>
            <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                <div class="item active">
                    <?php if (has_post_thumbnail()) : ?>
                        <?php the_post_thumbnail('slider'); ?>
                        <div class="container">
                            <div class="carousel-caption">
                                <h1><a href="<?php the_permalink(); ?>"><?php the_titlesmall('', '[+]', true, '35') ?></a></h1>
                                <p><?php echo excerpt(20); ?></p>
                            </div>
                        </div>
                    <?php else : ?>
                    <?php endif; ?>
                </div>
            <?php endwhile; ?>
            <?php else: ?>
            <?php endif; ?>

            <?php query_posts('category_name=slider&posts_per_page=2&offset=1'); ?>
            <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                <div class="item">
                    <?php if (has_post_thumbnail()) : ?>
                        <?php the_post_thumbnail('slider'); ?>
                        <div class="container">
                            <div class="carousel-caption">
                                <h1><a href="<?php the_permalink(); ?>"><?php the_titlesmall('', '[+]', true, '35') ?></a></h1>
                                <p><?php echo excerpt(20); ?></p>
                            </div>
                        </div>
                    <?php else : ?>
                    <?php endif; ?>
                </div>
            <?php endwhile; ?>
            <?php else: ?>
            <?php endif; ?>
        </div>
        <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div><!-- /.carousel -->
</div>

<!-- Espaçamento
================================================== -->
<div class="space-50"></div>
<div class="space-200"></div>