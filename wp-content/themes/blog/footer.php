<?php
/**
 * Created by PhpStorm.
 * User: Henry Jr
 * Date: 21/03/2016
 * Time: 16:46
 */
?>
</div>
<footer class="home">
    <div class="container">
        <span class="space-100"></span>
        <div class="row">
            <div class="col-sm-3">
                <span class="space-50"></span>
                <a href="<?php bloginfo('url'); ?>"><img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/logoFooter.png"></a>
            </div>
            <div class="col-sm-3">
                <?php
                wp_nav_menu(array(
                    'theme_location' => 'primary',
                    'menu' => 'footer-menu1',
                    'menu_class' => 'nav-footer',
                    'container' => 'false'
                ));
                ?>
            </div>
            <div class="col-sm-3"><?php
                wp_nav_menu(array(
                    'theme_location' => 'secundary',
                    'menu' => 'footer-menu2',
                    'menu_class' => 'nav-footer',
                    'container' => 'false'
                ));
                ?></div>
            <div class="col-sm-3"><?php
                wp_nav_menu(array(
                    'theme_location' => 'social',
                    'menu' => 'footer-social',
                    'menu_class' => 'nav-social',
                    'container' => 'false'
                ));
                ?></div>
        </div>
    </div>
</footer>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="<?php echo esc_url(get_template_directory_uri()); ?>/bootstrap/docs/assets/js/vendor/jquery.min.js"><\/script>')</script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="<?php echo esc_url(get_template_directory_uri()); ?>/bootstrap/dist/js/bootstrap.min.js"></script>
<?php wp_footer(); ?>
</body>
</html>
