<?php
/**
 * Created by PhpStorm.
 * User: Henry Jr
 * Date: 21/03/2016
 * Time: 16:46
 */
?>

<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo('charset') ?>"/>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <?php if (is_category() || is_tag() || is_month() || is_paged() || is_404()) { ?>
        <link rel="canonical" href="<?php echo home_url(); ?>"/>
        <meta name="robots" content="noindex, follow"/>
    <?php } ?>
    <?php if (is_search()) { ?>
        <meta name="robots" content="noindex, nofollow"/>
    <?php } ?>
    <?php if (is_singular()) { ?>
        <link rel="canonical" href="<?php the_permalink(); ?>"/>
    <?php } ?>
    <title><?php if (is_home()) {
            echo bloginfo('name');
        } elseif (is_404()) {
            echo bloginfo('name') . ' | ' . '404 Not Found';
        } elseif (is_category()) {
            echo bloginfo('name') . ' | ' . 'Categoria:';
            wp_title('');
        } elseif (is_search()) {
            echo bloginfo('name') . ' | ' . 'Busca';
        } elseif (is_day() || is_month() || is_year()) {
            echo bloginfo('name') . ' | ' . 'Arquivos:';
            wp_title('');
        } else {
            echo bloginfo('name') . ' | ' . wp_title('');
        } ?></title>
    <link rel="dns-prefetch" href="http://google-analytics.com"/>
    <link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name'); ?> RSS Feed" href="<?php bloginfo('rss2_url'); ?>"/>
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>"/>
    <!-- Favicons -->
    <link rel="apple-touch-icon-precomposed" href="<?php echo get_template_directory_uri(); ?>/images/favicon.jpg">
    <link rel="icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico">
    <!-- Bootstrap -->
    <link href="<?php echo esc_url(get_template_directory_uri()); ?>/bootstrap/dist/css/bootstrap.css" rel="stylesheet">
    <!-- Carousel -->
    <link href="<?php echo esc_url(get_template_directory_uri()); ?>/bootstrap/docs/examples/carousel/carousel.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="<?php echo esc_url(get_template_directory_uri()); ?>/bootstrap/docs/assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">
    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]>
    <script src="<?php echo esc_url(get_template_directory_uri()); ?>/bootstrap/docs/assets/js/ie8-responsive-file-warning.js"></script>
    <![endif]-->
    <script src="<?php echo esc_url(get_template_directory_uri()); ?>/bootstrap/docs/assets/js/ie-emulation-modes-warning.js"></script>
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <script>
        window.fbAsyncInit = function() {
            FB.init({
                appId      : '189463191439580',
                xfbml      : true,
                version    : 'v2.5'
            });
        };
        (function(d, s, id){
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) {return;}
            js = d.createElement(s); js.id = id;
            js.src = "//connect.facebook.net/en_US/sdk.js";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>
    <meta property="og:type" content="article" />

    <?php wp_head(); ?>

</head>
<body>
<div id="fb-root"></div>
<script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.5&appId=189463191439580";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
<div class="container">

    <header>
        <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/bgTop.jpg">
        <div class="logo">
            <a href="<?php bloginfo('url'); ?>" title="<?php bloginfo('name'); ?>"><?php bloginfo('name'); ?></a>
        </div>
        <?php require_once('wp_bootstrap_navwalker.php'); ?>
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                            data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse navtop" id="bs-example-navbar-collapse-1">
                    <?php
                    wp_nav_menu(array(
                        'theme_location' => 'primary',
                        'menu' => 'header-menu',
                        'menu_class' => 'nav navbar-nav',
                        'container' => 'false',
                        'walker' => new wp_bootstrap_navwalker()
                    ));
                    ?>
                </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
        </nav>
    </header>