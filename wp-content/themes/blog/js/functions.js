/**
 * Created by Henry Jr on 21/03/2016.
 */

$(document).ready(function () {
    $.each($('#navbar').find('li'), function () {
        $(this).toggleClass('active',
            $(this).find('a').attr('href') == window.location.pathname);
    });
	
	$('.carousel').carousel();
	
});