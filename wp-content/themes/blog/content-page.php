<?php
/**
 * The template for displaying page content. Used for page.
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */
?>

<article <?php post_class() ?> id="post-<?php the_ID(); ?>" role="article">
    <header role="banner" class="posthome">
        <h1 class="entry-title" title="<?php the_title(); ?>" role="heading" aria-level="1">
            <a href="<?php the_permalink(); ?>" alt="<?php the_title(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a>
        </h1>
        <small class="fleft">
            <span class="label label-danger">
                <?php edit_post_link(__('<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Editar', '')); ?></span>
        </small>
        <span class="space-25"></span>
    </header>
    <div id="post" class="post-content entry-content posthome">
        <section class="post-container">
            <?php the_content('<p>Leia o restante deste post &raquo;</p>'); ?>
        </section>
    </div>
    <!-- .post-content -->
    <footer role="contentinfo">
        <?php wp_link_pages(); ?>
    </footer>
</article>
<!-- .post -->