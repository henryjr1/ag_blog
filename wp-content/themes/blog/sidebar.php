<?php
/**
 * The sidebar containing the main widget area.
 *
 * If no active widgets in sidebar, let's hide it completely.
 *
 * @package WordPress
 * @subpackage wbruno
 * @since wbruno 0.0.1
 */
?>
<!-- facebook
            ================================================== -->
<div class="fb">
    <h3>Facebook</h3>
    <div>
        <?php dynamic_sidebar( 'facebook' ); ?>
    </div>
</div>

<!-- instagram
================================================== -->
<div class="insta">
    <h3>Instagram</h3>
    <div>
        <?php dynamic_sidebar( 'instagram' ); ?>
    </div>
</div>

<!-- Biografia
================================================== -->
<div class="tagline">
    <h3>Biografia</h3>
    <div class="bio">
        <a href="">Mãe e filha apaixonadas pela vida</a>
    </div>
</div>

<!-- Visualizações
================================================== -->
<div class="tagline">
    <h3>Visualizações</h3>
    <div>
        <?php dynamic_sidebar( 'stats' ); ?>
    </div>
</div>

<!-- Publicidade
================================================== -->
<div class="tagline">
    <h3>Publicidade</h3>
    <div>
        <?php include('inc/publicidade.php'); ?>
    </div>
</div>

<!-- Newsletter
================================================== -->
<div class="newsletter">
    <h3>Newsletter</h3>
    <div>
        <?php dynamic_sidebar( 'newsletter' ); ?>
    </div>
</div><!-- #sidebar -->