<?php
/**
 * The template for displaying single content. Used for single.
 *
 * @package WordPress
 * @subpackage wbruno
 * @since wbruno 0.0.1
 */
?>

<article <?php post_class() ?> id="post-<?php the_ID(); ?>" itemscope itemtype="http://schema.org/Article" role="article">
    <header role="banner" class="posthome">
        <?php echo lw_date(); ?>
        <h3 class="entry-title2" role="heading" aria-level="2" itemprop="name">
            <a href="<?php the_permalink(); ?>" alt="<?php the_title(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a>
        </h3>
        <small class="fleft">
            <span class="label label-danger">
                <?php edit_post_link(__('<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> Editar', 'wbruno')); ?></span>
            <span class="vcard author" itemprop="author" itemscope itemtype="http://schema.org/Person">
                 Postado por <?php the_author_posts_link(); ?></span>
            </span> | Categoria: <span itemprop="articleSection"><?php the_category(', ') ?></span>
        </small>
    </header>
    <div id="post" class="post-content entry-content posthome">
        <span class="space-25"></span>
        <section class="post-container">
            <?php echo excerpt(50); ?>
        </section>
        <!-- .post-content -->
        <footer role="contentinfo">
            <p>
                <?php the_tags(); ?>
            </p>
            <?php wp_link_pages(); ?>
        </footer>
    </div>
    <!-- Espaçamento
    ================================================== -->
    <div class="space-25"></div>
</article>
<!-- .post -->
